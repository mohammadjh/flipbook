(function(){
    'use strict';

    angular
        .module('anguFlipBook.service')
        .factory('Book', Book);

    Book.$inject = ['$http'];

    function Book($http) {
        var Book = {
            pages: pages,
            page: page
        };

        return Book;

        function pages(callback) {
            return $http.get('book.json').then(callback, errorFn);

            function errorFn(status, data, config){
                console.log(error);
            }
        }

        function page(id){
            return $http.get('book.json', id);
        }
    }
})();