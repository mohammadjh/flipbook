(function(){
    'use strict';

    angular
        .module('anguFlipBook', [
            'anguFlipBook.controller',
            'anguFlipBook.directive',
            'anguFlipBook.service'
            ]);



    angular
        .module('anguFlipBook.controller', []);


    angular
        .module('anguFlipBook.directive', []);

    angular
        .module('anguFlipBook.service', []);

})();