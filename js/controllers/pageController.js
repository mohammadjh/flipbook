(function(){
    'use strict';

    angular
        .module('anguFlipBook.controller')
        .controller('pageController', pageController);

    pageController.$inject = ['$scope', 'Book'];

    function pageController($scope, Book) {
        $scope.page = page;
        $scope.id = 3;

        function page(){
            Book.page($scope.id).then(successFn, errorFn);

            function successFn(data, status, config, response) {
                console.log('Success', data);
            }

            function errorFn(data, status, config, response) {
                console.log('Error', data);
            }
        }
    }
})();