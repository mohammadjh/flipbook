(function(){
    'use strict';

    angular
        .module('anguFlipBook.controller')
                .controller('BookController', BookController);

    BookController.$inject = ['Book', '$scope'];

    function BookController(Book, $scope){
        $scope.goTo = goTo;
        $scope.zoomView = zoomView;
        $scope.page = page;

        /*function get(){
            Book.pages().then(successFn, errorFn);

            function successFn(data, status, response, config){
               console.log('success', data);
               $scope.book = data;
               console.log($scope.book);
            }

            function errorFn(error){
                console.log('error', error);
            }
        }*/

        Book.pages(function(data) {
            $scope.book = data.data;
        })

        function page(id) {
            Book.page(id).then
        }

        function next(){
            $('#flipbook').turn('next');
        }

        function goTo(page){
             $('#flipbook').turn('page', page);
        }

        function zoomView(){
            $("#flipbook").turn("zoom", 0.5);
        }

        $scope.alert = function(){
            alert('Hello');
        }
    }
})();