(function() {
    'use strict';

    angular
        .module('anguFlipBook.directive')
        .directive('flipbook', function(){
          return{
            restrict: 'E',
            replace: true,
            compile: function(element, attrs){
              element.turn({
                width: '300px',
                height: '300px',
                display: 'double',
                gradients: true,
                autoCenter: true,
                elevation:50,
                when: {
                    turned: function(e, page) {
                        console.log('Current view: ', $(this).turn('view'));
                        console.log('Current page: ', $(this).turn("page"));
                        var currPage = $(this).turn("page");
                        console.log(currPage);

                        if (currPage%2 == 0)
//                            element.addClass('even');
                            alert('Even');
                        else
                            alert('odd');
//                            element.addClass('odd');
                        }
                    }
              });

//              element.addClass('flipbook');
              element.attr('id', 'flipbook');

              return function(scope, el) {
                el.on('click', '[data-page]', function(e){
                  el.turn('page', $(e.target).data('page'));
                });
              };
            },
            templateUrl: "flipbook.html"
          }
        });
})();